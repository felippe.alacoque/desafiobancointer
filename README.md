<h1>API dígito Único</h1>

- Aplicação construída com banco de dados **H2** (para acessar o console: localhost:8080/h2), utilizando Maven;
- Testes unitários construídos com **JUnit4**
- Coleção de dados para testes com **POSTMAN** disponibilizada no arquivo **postman_colletion.json**;
- A documentação da API pode ser acessada no endereço **localhost:8080/swagger-ui.html**;
- IDE STS 3.9.5 utilizada para desenvolvimento do projeto em liguagem JAVA com **Spring BOOT** versão 2.0.3.RELEASE;

**NOTA:** Criptografia não implementada totalmente. Feito de acordo com pesquisa realizada.