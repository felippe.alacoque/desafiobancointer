package com.inter.desafio.exception;

public class DigitoUnicoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DigitoUnicoException(String errorMessage) {
		super(errorMessage);
	}

}
